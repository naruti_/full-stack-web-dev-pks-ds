<?php
abstract class Hewan
{
    protected $nama;
    protected $darah;
    protected $jumlahKaki;
    protected $keahlian;

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->darah = 50;
        // $this->jumlahKaki = $jumlahKaki;
        // $this->keahlian = $keahlian;
    }
    public function getNama()
    {
        return $this->nama;
    }
    public function getDarah()
    {
        return $this->darah;
    }
    public function getJumlahKaki()
    {
        return $this->jumlahKaki;
    }
    public function getKeahlian()
    {
        return $this->keahlian;
    }
    public function setNama($nama)
    {
        $this->nama = $nama;
    }
    public function setDarah($darah)
    {
        $this->darah = $darah;
    }
    public function setJumlahKaki($jumlahKaki)
    {
        $this->jumlahKaki = $jumlahKaki;
    }
    public function setKeahlian($keahlian)
    {
        $this->keahlian = $keahlian;
    }

    abstract public function atraksi();
    abstract public function getInfoHewan();
};

trait Fight
{
    protected $attackPower;
    protected $defensePower;

    public function getAttackPower()
    {
        return $this->attackPower;
    }
    public function getDefensePower()
    {
        return $this->defensePower;
    }
    public function setAttackPower($attackPower)
    {
        $this->attackPower = $attackPower;
    }
    public function setDefensePower($defensePower)
    {
        $this->defensePower = $defensePower;
    }

    public function serang($hewan)
    {
        echo $this->getNama() . " sedang menyerang " . $hewan->getNama();
        echo PHP_EOL;
        $hewan->diserang($this);
    }
    public function diserang($hewan)
    {
        echo  $this->getNama() . " sedang diserang " . $hewan->getNama();
        echo PHP_EOL;
        $darah = $this->getDarah();
        // echo "darah sebelumnya" . $darah;
        // echo PHP_EOL;
        $darah = $darah - ($hewan->getAttackPower() / $this->getDefensePower());
        echo $this->getNama() . " memiliki sisa darah " . $darah;
        $this->setDarah($darah);
    }
};

class Elang extends Hewan
{
    use Fight;
    public function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defensePower = 5;
    }
    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian;
    }
    public function getInfoHewan()
    {
        echo "Nama : " . $this->getNama() . PHP_EOL;
        echo "Darah : " . $this->getDarah() . PHP_EOL;
        echo "Jumlah Kaki : " . $this->getJumlahKaki() . PHP_EOL;
        echo "Keahlian : " . $this->getKeahlian() . PHP_EOL;
        echo "Attack Power : " . $this->getAttackPower() . PHP_EOL;
        echo "Defense Power : " . $this->getDefensePower() . PHP_EOL;
    }
}


class Harimau extends Hewan
{
    use Fight;
    public function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defensePower = 8;
    }
    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian;
    }
    public function getInfoHewan()
    {
        echo "Nama : " . $this->getNama() . PHP_EOL;
        echo "Darah : " . $this->getDarah() . PHP_EOL;
        echo "Jumlah Kaki : " . $this->getJumlahKaki() . PHP_EOL;
        echo "Keahlian : " . $this->getKeahlian() . PHP_EOL;
        echo "Attack Power : " . $this->getAttackPower() . PHP_EOL;
        echo "Defense Power : " . $this->getDefensePower() . PHP_EOL;
    }
}

$elang = new Elang("Elang 1");
$harimau = new Harimau("Harimau 1");
echo $elang->getNama();
echo PHP_EOL;
echo $elang->atraksi();
echo PHP_EOL;
echo PHP_EOL;

// Simulasi serang
echo $elang->serang($harimau);
echo PHP_EOL;
// echo $harimau->getDarah();

$elang->getInfoHewan();
echo PHP_EOL;
$harimau->getInfoHewan();
echo PHP_EOL;