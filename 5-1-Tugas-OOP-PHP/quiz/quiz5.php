<?php

class Hewani
{
    private $nama;
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
}

class Komodo extends Hewani
{
    public function getNama () {
        return $this->nama;
    }
}

$komodo = new Komodo("test komodo");
echo $komodo->getNama();