//soal1
console.log("Jawaban no 1 \n");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
daftarHewan.forEach((item, index) => {
    console.log(item);
})
console.log("\n");

//soal2
console.log("Jawaban no 2 \n");
var data = {name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming"};
console.log(`Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`);
console.log("\n");


//soal3
console.log("Jawaban no 3 \n");
function hitung_huruf_vokal(sentence) {
    let countVokal = 0
    let vokal = ["a", "i", "u", "e", "o"];
    for (let i = 0; i < sentence.length; i++) {
        // console.log(vokal.indexOf(sentence[i]));
        if(vokal.indexOf(sentence[i]) > -1) {
            countVokal++
            // console.log("countVokal:" + countVokal);
        }   
    }
    return countVokal
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

console.log("\n");


//soal4
console.log("Jawaban no 4 \n");

let hitung = (angka) => {
    let hasil = -2
    for (let i = 0; i < angka; i++) {
        hasil += 2
    }
    return hasil
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

console.log("\n");