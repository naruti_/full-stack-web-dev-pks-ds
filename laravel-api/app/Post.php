<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Post extends Model
{
    // protected $test = Str::afterLast($subject, 'search');
    protected $fillable = ['title', 'description'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }

    public function comments() {
        return $this->hasMany('App\Comment');
    }
}
