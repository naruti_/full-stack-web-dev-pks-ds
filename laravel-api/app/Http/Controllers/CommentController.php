<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
        // return $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments = Comment::where('post_id', $post_id)->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Posts retrieved successfully',
            'data' => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = \Validator::make($requestAll, [
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);
        // $post = Comment::create($requestAll);
        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment created successfully',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment could not be created',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment retrieved successfully',
                'data' => $comment
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment with id ' . $id . 'not found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $comment = Comment::find($id);
        // $post = Comment::create($requestAll);
        if ($comment) {
            $comment->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment updated successfully',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment with id' . $id . 'could not be found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        // $post = Comment::create($requestAll);
        if ($comment) {
            $comment->delete();
            return response()->json([
                'success' => true,
                'message' => 'Comment deletd successfully',
                // 'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment with id' . $id . 'could not be found',
        ], 404);
    }
}
