<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PostController extends Controller
{
    public function __construct()
    {
        // return $this->middleware('auth:api')->except(['index', 'show']);
        return $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Posts retrieved successfully',
            'data' => $posts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $post = Post::create([
        //     'title' => $request->title,
        //     'description' => $request->description
        // ]);
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'title' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $post = Post::create($requestAll);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post created successfully',
                'data' => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Post could not be created',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post retrieved successfully',
                'data' => $post
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post with id ' . $id . 'not found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $post = Post::find($id);
        // $post = Post::create($requestAll);
        if ($post) {
            $post->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post updated successfully',
                'data' => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Post with id' . $id . 'could not be found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        // $post = Post::create($requestAll);
        if ($post) {
            $post->delete();
            return response()->json([
                'success' => true,
                'message' => 'Post deletd successfully',
                // 'data' => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Post with id' . $id . 'could not be found',
        ], 404);
    }
}
