<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Roles retrieved successfully',
            'data' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $role = Role::create($requestAll);
        if ($role) {
            return response()->json([
                'success' => true,
                'message' => 'Role created successfully',
                'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role could not be created',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $role = Role::find($id);
        if ($role) {
            return response()->json([
                'success' => true,
                'message' => 'Role retrieved successfully',
                'data' => $role
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role with id ' + $id + 'not found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $role = Role::find($id);
        // $role = Role::create($requestAll);
        if ($role) {
            $role->update([
                'name' => $request->name,

            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role updated successfully',
                'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role with id' + $id + 'could not be found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        // $role = Role::create($requestAll);
        if ($role) {
            $role->delete();
            return response()->json([
                'success' => true,
                'message' => 'Role deletd successfully',
                // 'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role with id' + $id + 'could not be found',
        ], 404);
    }
}
