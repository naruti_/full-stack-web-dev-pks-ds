<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\OtpCode;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;



class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        // dd('test');
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $otp_code = OtpCode::where('otp', $requestAll['otp'])->first();
        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code is not valid',
            ], 400);
        }

        $user = $otp_code->user;
        if($otp_code->valid_until < Carbon::now()) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code has expired',
                'errors' => [
                    'otp' => 'OTP Code has expired'
                ]
            ], 404);
        }
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);
        $otp_code->delete();
        return response()->json([
            'success' => true,
            'message' => 'Congratulations, your account has been verified',
            'data' => [
                'user' => $user
            ]
        ], 201);
        // dd($user);
    }
}
