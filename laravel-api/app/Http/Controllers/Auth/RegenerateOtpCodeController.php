<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'email' => 'required'
            // 'username' => 'required|unique:users,username',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $requestAll['email'])->first();
        if ($user->otpCode) {
            $user->otpCode->delete();
        }

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
            // dd($check);
        } while ($check);
        $validUntil = Carbon::now()->addMinutes(15);
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'OTP Code has been regenerated, please check your email',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 201);
        dd($user->otpCode);
    }
}
