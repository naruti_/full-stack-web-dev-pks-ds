<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('test');
        $requestAll = $request->all();
        // dd($requestAll);
        $validator = Validator::make($requestAll, [
            'name' => 'required',
            // 'email' => 'required|email|unique:users',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $user = User::create($requestAll);
        //menit 28:38
        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
            // dd($check);
        } while ($check);
        $validUntil = Carbon::now()->addMinutes(15);
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User Created, please check your email for OTP code',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 201);
        //menit 46:41
    }
}
