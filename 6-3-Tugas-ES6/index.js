//soal 1
console.log("Jawaban no 1 \n");
let luasPersegiPanjang = (panjang, lebar) => {
  return panjang * lebar;
};

let kelilingPersegiPanjang = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};
console.log(luasPersegiPanjang(5, 6));
console.log(kelilingPersegiPanjang(5, 6));
console.log("\n");

//soal 2
console.log("Jawaban no 2 \n");
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function () {
      console.log(firstName + " " + lastName);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();
console.log("\n");

//soal 3
console.log("Jawaban no 3 \n");
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);

console.log("\n");

//soal 4
console.log("Jawaban no 4 \n");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);
console.log("\n");

//soal 5
console.log("Jawaban no 5 \n");
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)
console.log("\n");
